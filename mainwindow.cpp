#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _socket = new QTcpSocket(this);
    timer = new QTimer;
    ui->dir_combobox->addItem("Forward");
    ui->dir_combobox->addItem("Backward");
    ui->dir_combobox->addItem("Left");
    ui->dir_combobox->addItem("Right");
    ui->dir_combobox->addItem("For_Left");
    ui->dir_combobox->addItem("For_Right");
    ui->dir_combobox->addItem("Back_Left");
    ui->dir_combobox->addItem("Back_Right");
    ui->time_lineedit->setValidator(new QIntValidator(0, 99, this));
    ui->traectory_command_label->setText(generation());
    connect(_socket, SIGNAL(readyRead()), this, SLOT(SocketReadyRead()));
    connect(_socket, SIGNAL(connected()), this, SLOT(SocketConnected()));
    connect(_socket, SIGNAL(disconnected()), this, SLOT(SocketDisconnected()));
    connect(_socket, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(SocketDisplayError(QAbstractSocket::SocketError)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::send_once(QString &some_command){
    ui->log_browser->append("Sending:" + some_command);
    _socket->write(some_command.toStdString().c_str());
    some_command = "@@$$";
}

void MainWindow::end_timer(){
    timer_flag = 1;
    if(traectory_command_vec.size() != 0 || timer_loop == 1){
        switch(timer_loop){
        case 0:
            timer_loop = 1;
            traectory_command = traectory_command_vec[0];
            if(traectory_command_vec.size() > 1 && traectory_command.size() == traectory_command_vec[1].size() && traectory_command.at(2) == traectory_command_vec[1].at(2)){
                same_command_flag = 1;
            }
            else{
                same_command_flag = 0;
            }
            send_once(traectory_command);
            traectory_command_vec.erase(traectory_command_vec.begin());
        {
            int seconds = time_vec[0]*1000;
            time_vec.erase(time_vec.begin());
            QTimer::singleShot(seconds, this, SLOT(end_timer()));
        }
            break;
        case 1:
            timer_loop = 0;
            if(!same_command_flag){
                traectory_command = "@@Wstop$$";
                send_once(traectory_command);
            }
            QTimer::singleShot(500, this, SLOT(end_timer()));
            break;
        }
    }
    else{
        timer_flag = 0;
        timer_loop = 0;
        traectory_command = generation();
    }
}

void MainWindow::sending(char dir_char, int dir_slider_val, bool &flag){
    if(dir_slider_val >= 10){
        command.insert(2, dir_char + QString::number(dir_slider_val));
    }
    else{
        command.insert(2, dir_char + QString::number(0) + QString::number(dir_slider_val));
    }
    send_once(command);
    flag = 1;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        switch(event->key()){
        case Qt::Key_W:
            if(!key_u && !key_d){
                sending('U', ui->u_d_slider->value(), key_u);
            }
            break;
        case Qt::Key_S:
            if(!key_u && !key_d){
                sending('D', ui->u_d_slider->value(), key_d);
            }
            break;
        case Qt::Key_A:
            if(!key_o && !key_c){
                sending('O', ui->o_c_slider->value(), key_o);
            }
            break;
        case Qt::Key_D:
            if(!key_o && !key_c){
                sending('C', ui->o_c_slider->value(), key_c);
            }
            break;
        case Qt::Key_I:
            if(!key_f && !key_b){
                sending('F', ui->f_b_slider->value(), key_f);
            }
            break;
        case Qt::Key_K:
            if(!key_f && !key_b){
                sending('B', ui->f_b_slider->value(), key_b);
            }
            break;
        case Qt::Key_J:
            if(!key_l && !key_r){
                sending('L', ui->l_r_slider->value(), key_l);
            }
            break;
        case Qt::Key_L:
            if(!key_l && !key_r){
                sending('R', ui->l_r_slider->value(), key_r);
            }
            break;
        }
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(event->isAutoRepeat()){
            event->ignore();
        }
        else{
            if((event->key() == Qt::Key_W && key_u)
                    || (event->key() == Qt::Key_A && key_o)
                    || (event->key() == Qt::Key_S && key_d)
                    || (event->key() == Qt::Key_D && key_c)){
                command = "@@Lstop$$";
                send_once(command);
                key_u = key_d = key_o = key_c = 0;
            }
            else if((event->key() == Qt::Key_I && key_f)
                    || (event->key() == Qt::Key_J && key_l)
                    || (event->key() == Qt::Key_K && key_b)
                    || (event->key() == Qt::Key_L && key_r)){
                command = "@@Wstop$$";
                send_once(command);
                key_f = key_b = key_l = key_r = 0;
            }
        }
    }
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_u_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_u && !key_d){
            sending('U', ui->u_d_slider->value(), key_u);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else {
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_d_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_u && !key_d){
            sending('D', ui->u_d_slider->value(), key_d);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_o_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_o && !key_c){
            sending('O', ui->o_c_slider->value(), key_o);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_c_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_o && !key_c){
            sending('C', ui->o_c_slider->value(), key_c);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_f_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_f && !key_b){
            sending('F', ui->f_b_slider->value(), key_f);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_b_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_f && !key_b){
            sending('B', ui->f_b_slider->value(), key_b);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_l_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_l && !key_r){
            sending('L', ui->l_r_slider->value(), key_l);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_r_button_pressed()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(!key_l && !key_r){
            sending('R', ui->l_r_slider->value(), key_r);
        }
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_u_d_slider_valueChanged(int value)
{
    ui->u_d_spinbox->setValue(value);
}

void MainWindow::on_u_d_spinbox_valueChanged(int arg1)
{
    ui->u_d_slider->setValue(arg1);
}

void MainWindow::on_o_c_slider_valueChanged(int value)
{
    ui->o_c_spinbox->setValue(value);
}

void MainWindow::on_o_c_spinbox_valueChanged(int arg1)
{
    ui->o_c_slider->setValue(arg1);
}

void MainWindow::on_f_b_slider_valueChanged(int value)
{
    ui->f_b_spinbox->setValue(value);
}

void MainWindow::on_f_b_spinbox_valueChanged(int arg1)
{
    ui->f_b_slider->setValue(arg1);
}

void MainWindow::on_l_r_slider_valueChanged(int value)
{
    ui->l_r_spinbox->setValue(value);
}

void MainWindow::on_l_r_spinbox_valueChanged(int arg1)
{
    ui->l_r_slider->setValue(arg1);
}

void MainWindow::on_connect_button_clicked()
{
   _socket->connectToHost(ipaddr,portaddr.toInt());
}

void MainWindow::on_ip_edit_textEdited(const QString &arg1)
{
    if(!arg1.contains(':')){
        ipaddr = arg1;
    }
    else{
        portaddr = arg1.section(':',1,1);
        ipaddr = arg1.section(':', 0, 0);
    }
    ui->ip_label->setText(ipaddr);
    ui->port_label->setText(portaddr);
}

void MainWindow::SocketDisplayError(QAbstractSocket::SocketError socketError)
 {
     switch (socketError) {
     case QAbstractSocket::RemoteHostClosedError:
         break;
     case QAbstractSocket::HostNotFoundError:
         QMessageBox::information(this, tr("Boteon: Controller"),
                                  tr("The host was not found. Please check the "
                                     "host name and port settings."));
         break;
     case QAbstractSocket::ConnectionRefusedError:
         QMessageBox::information(this, tr("Boteon: Controller"),
                                  tr("The connection was refused by the peer. "
                                     "Make sure the robot server is running, "
                                     "and check that the host name and port "
                                     "settings are correct."));
         break;
     default:
         QMessageBox::information(this, tr("Boteon: Controller"),
                                  tr("The following error occurred: %1.")
                                  .arg(_socket->errorString()));
     }
 }

void MainWindow::SocketConnected(){
    ui->stat_label->setText("Connected");
    ui->log_browser->append("Connection success!");
}

void MainWindow::SocketDisconnected(){
    ui->stat_label->setText("Disconnected");
    ui->log_browser->append("Connection lost");
}

void MainWindow::SocketReadyRead(){
    ui->stat_label->setText("Reading");
}

void MainWindow::on_send_button_clicked()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        command = ui->command_edit->text();
        send_once(command);
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_u_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_u || key_d || key_o || key_c){
            command = "@@Lstop$$";
            send_once(command);
            key_u = key_d = key_o = key_c = 0;
        }
    }
}

void MainWindow::on_d_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_u || key_d || key_o || key_c){
            command = "@@Lstop$$";
            send_once(command);
            key_u = key_d = key_o = key_c = 0;
        }
    }
}

void MainWindow::on_o_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_u || key_d || key_o || key_c){
            command = "@@Lstop$$";
            send_once(command);
            key_u = key_d = key_o = key_c = 0;
        }
    }
}

void MainWindow::on_c_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_u || key_d || key_o || key_c){
            command = "@@Lstop$$";
            send_once(command);
            key_u = key_d = key_o = key_c = 0;
        }
    }
}

void MainWindow::on_f_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_f || key_b || key_l || key_r){
            command = "@@Wstop$$";
            send_once(command);
            key_f = key_b = key_l = key_r = 0;
        }
    }
}

void MainWindow::on_b_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_f || key_b || key_l || key_r){
            command = "@@Wstop$$";
            send_once(command);
            key_f = key_b = key_l = key_r = 0;
        }
    }
}

void MainWindow::on_l_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_f || key_b || key_l || key_r){
            command = "@@Wstop$$";
            send_once(command);
            key_f = key_b = key_l = key_r = 0;
        }
    }
}

void MainWindow::on_r_button_released()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        if(key_f || key_b || key_l || key_r){
            command = "@@Wstop$$";
            send_once(command);
            key_f = key_b = key_l = key_r = 0;
        }
    }
}

void MainWindow::on_generate_button_clicked()
{
    traectory_command = generation();
    ui->traectory_command_label->setText(traectory_command);
}

QString MainWindow::generation(){
    if(traectory_command.size() == 4){
        switch(ui->dir_combobox->currentIndex()){
        case 0:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'F' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "F0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 1:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'B' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "B0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 2:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'L' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "L0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 3:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'R' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "R0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 4:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'F' + QString::number(ui->step_spinbox->value()) + 'L' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "F0" + QString::number(ui->step_spinbox->value()) + "L0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 5:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'F' + QString::number(ui->step_spinbox->value())   + 'R' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "F0" + QString::number(ui->step_spinbox->value()) + "R0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 6:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'B' + QString::number(ui->step_spinbox->value()) + 'L' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "B0" + QString::number(ui->step_spinbox->value())+ "L0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        case 7:
            if(ui->step_spinbox->value() >= 10){
                return traectory_command.insert(2, 'B' + QString::number(ui->step_spinbox->value()) + 'R' + QString::number(ui->step_spinbox->value()));
            }
            else{
                return traectory_command.insert(2, "B0" + QString::number(ui->step_spinbox->value()) + "R0" + QString::number(ui->step_spinbox->value()));
            }
            break;
        }
    }
    else{
        traectory_command = "@@$$";
        return generation();
    }
    return 0;
}

void MainWindow::on_go_button_clicked()
{
    if(ui->stat_label->text() == "Connected" && !timer_flag){
        ui->traectory_browser->append("Closed loop!");
        QTimer::singleShot(1,this, SLOT(end_timer()));
        ui->traectory_browser->append("");
        ui->traectory_browser->append("Next loop:");
    }
    else if(timer_flag){
        ui->log_browser->append("Wait for the end of traectory");
    }
    else{
        ui->log_browser->append("Connect to robot first!");
    }
}

void MainWindow::on_disconnect_button_clicked()
{
    _socket->disconnectFromHost();
}

void MainWindow::on_add_button_clicked()
{
    traectory_command_vec.push_back(traectory_command);
    time_vec.push_back(ui->time_lineedit->text().toInt());
    ui->traectory_browser->append(traectory_command + ", " + ui->time_lineedit->text() + "sec;");
}

void MainWindow::on_dir_combobox_currentIndexChanged()
{
    traectory_command = generation();
    ui->traectory_command_label->setText(traectory_command);
}

void MainWindow::on_step_spinbox_valueChanged()
{
    traectory_command = generation();
    ui->traectory_command_label->setText(traectory_command);
}

void MainWindow::on_delete_button_clicked()
{
    if(traectory_command_vec.size() > 0){
        QTextCursor cursor = ui->traectory_browser->textCursor();
        cursor.movePosition(QTextCursor::End);
        cursor.select(QTextCursor::LineUnderCursor);
        cursor.removeSelectedText();
        cursor.deletePreviousChar(); // Added to trim the newline char when removing last line
        ui->traectory_browser->setTextCursor(cursor);
        traectory_command_vec.erase(traectory_command_vec.end() - 1);
        time_vec.erase(time_vec.end() - 1);
    }
}
