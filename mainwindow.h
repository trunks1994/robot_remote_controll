#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QMessageBox>
#include <QByteArray>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <QTimer>
#include <QTime>
#include <QSlider>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private slots:
    QString generation();
    void end_timer();
    void sending(char dir_char, int dir_slider_val, bool &flag);
    void send_once(QString &some_command);
    void SocketConnected();
    void SocketDisconnected();
    void SocketReadyRead();
    void SocketDisplayError(QAbstractSocket::SocketError socketError);

private:
    bool key_u = 0;
    bool key_d = 0;
    bool key_l = 0;
    bool key_r = 0;
    bool key_f = 0;
    bool key_b = 0;
    bool key_o = 0;
    bool key_c = 0;
    bool timer_flag = 0;
    bool same_command_flag = 0;
    int timer_loop = 0;
    QTcpSocket *_socket; //сокет
    QString ipaddr;
    QString portaddr;
    quint16 _blockSize;//текущий размер блока данных
    QString _name;//имя клиента
    QString command = "@@$$";
    QString traectory_command = "@@$$";
    std::vector<QString> traectory_command_vec;
    std::vector<int> time_vec;
    QTimer *timer;
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_u_button_pressed();

    void on_d_button_pressed();

    void on_o_button_pressed();

    void on_c_button_pressed();

    void on_f_button_pressed();

    void on_b_button_pressed();

    void on_l_button_pressed();

    void on_r_button_pressed();

    void on_u_d_slider_valueChanged(int value);

    void on_u_d_spinbox_valueChanged(int arg1);

    void on_o_c_slider_valueChanged(int value);

    void on_o_c_spinbox_valueChanged(int arg1);

    void on_f_b_slider_valueChanged(int value);

    void on_f_b_spinbox_valueChanged(int arg1);

    void on_l_r_slider_valueChanged(int value);

    void on_l_r_spinbox_valueChanged(int arg1);

    void on_connect_button_clicked();

    void on_ip_edit_textEdited(const QString &arg1);

    void on_send_button_clicked();

    void on_u_button_released();

    void on_d_button_released();

    void on_o_button_released();

    void on_c_button_released();

    void on_f_button_released();

    void on_b_button_released();

    void on_l_button_released();

    void on_r_button_released();

    void on_generate_button_clicked();

    void on_go_button_clicked();

    void on_disconnect_button_clicked();

    void on_add_button_clicked();

    void on_dir_combobox_currentIndexChanged();

    void on_step_spinbox_valueChanged();

    void on_delete_button_clicked();

private:
    Ui::MainWindow *ui;
};



#endif // MAINWINDOW_H
